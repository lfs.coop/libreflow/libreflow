import os

from kabaret import flow
from kabaret.flow_contextual_dict import get_contextual_dict

from .filesystemobject import (
    FileSystemObjectRevision,
    FileSystemObjectRevisionMap,
    TrackedFileSystemObject,
)


class CreateFileWorkingCopy(flow.Action):
    pass


class PublishFileChanges(flow.Action):
    pass


class OpenFile(flow.Action):
    pass


class File(TrackedFileSystemObject):

    extension = flow.Param("")

    create_working_copy = flow.Child(CreateFileWorkingCopy)
    publish_changes = flow.Child(PublishFileChanges)
    open = flow.Child(OpenFile)


class CreateFile(flow.Action):
    pass
