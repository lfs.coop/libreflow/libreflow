import os

from kabaret import flow
from kabaret.flow_contextual_dict import get_contextual_dict

from ..site import Site, SyncMap


class FileSystemObject(flow.Object):
        
    _parent = flow.Parent()

    path = flow.Computed(cached=True)
    full_name = flow.Param("")
    full_path = flow.Computed(cached=True)

    created = flow.Param().ui(editor="datetime")
    modified = flow.Param().ui(editor="datetime")
    exists = flow.Computed(cached=True).ui(editor="bool")
    read_only = flow.Computed().ui(editor="bool")

    created_by = flow.Param("")


class FileSystemObjectMap(flow.Map):

    @classmethod
    def mapped_type(cls):
        return flow.injection.injectable(FileSystemObject)


class FileSystemObjectRevision(FileSystemObject):
    
    hash = flow.Param("")
    comment = flow.Param("")
    sync = flow.Child(SyncMap)


class FileSystemObjectRevisionMap(flow.Map):
    
    @classmethod
    def mapped_type(cls):
        return flow.injectable(FileSystemObjectRevision)


class FileSystemObjectHistory(flow.Object):
    
    revisions = flow.Child(FileSystemObjectRevisionMap).injectable()
    head_name = flow.Computed()
    seen_name = flow.Computed()


class TrackedFileSystemObject(FileSystemObject):

    history = flow.Child(FileSystemObjectHistory).injectable()
    locked_by = flow.Param("")
    source_site = flow.Child(Site)


# TODO: See if File[Set|Sequence]Map[Item] can be factorized as FileMap[Item]
# Is there any advantage in FileSystemObject.[created_by|locked_by] being Connections instead of simple Params ?
# -> Injectability allows to easily override relations
# -> Params might not clearly define what users are identified by in the flow: elementary types (string, int) or high-level types which embed those identifiers internally
