from kabaret import flow

from .filesystemobject import TrackedFileSystemObject


class FileSequenceItem(flow.Object):
    
    index = flow.IntParam(0)


class FileSequenceMap(flow.Map):

    @classmethod
    def mapped_type(cls):
        return FileSequenceItem


class CreateFileSequenceWorkingCopy(flow.Action):
    pass


class PublishFileSequenceChanges(flow.Action):
    pass


class OpenFileSequence(flow.Action):
    pass


class FileSequence(TrackedFileSystemObject):

    extension = flow.Param("")
    files = flow.Child(FileSequenceMap)

    create_working_copy = flow.Child(CreateFileSequenceWorkingCopy)
    publish_changes = flow.Child(PublishFileSequenceChanges)
    open = flow.Child(OpenFileSequence)


class CreateFileSequence(flow.Action):
    pass
