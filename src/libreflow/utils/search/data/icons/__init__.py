from kabaret.app import resources
import logging

resources.add_folder('icons.search', __file__)
logging.debug('SEARCH ICONS LOADED')